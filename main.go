package main

import (
	"compress/gzip"
	"encoding/json"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"

	"github.com/streadway/amqp"
)

// MyJSONName struct for NVD FEED data
type MyJSONName struct {
	CVEItems []struct {
		Configurations struct {
			CVEDataVersion string `json:"CVE_data_version"`
			Nodes          []struct {
				CpeMatch []struct {
					Cpe22Uri   string `json:"cpe22Uri"`
					Cpe23Uri   string `json:"cpe23Uri"`
					Vulnerable bool   `json:"vulnerable"`
				} `json:"cpe_match"`
				Operator string `json:"operator"`
			} `json:"nodes"`
		} `json:"configurations"`
		Cve struct {
			CVEDataMeta struct {
				ASSIGNER string `json:"ASSIGNER"`
				ID       string `json:"ID"`
			} `json:"CVE_data_meta"`
			Affects struct {
				Vendor struct {
					VendorData []struct {
						Product struct {
							ProductData []struct {
								ProductName string `json:"product_name"`
								Version     struct {
									VersionData []struct {
										VersionAffected string `json:"version_affected"`
										VersionValue    string `json:"version_value"`
									} `json:"version_data"`
								} `json:"version"`
							} `json:"product_data"`
						} `json:"product"`
						VendorName string `json:"vendor_name"`
					} `json:"vendor_data"`
				} `json:"vendor"`
			} `json:"affects"`
			DataFormat  string `json:"data_format"`
			DataType    string `json:"data_type"`
			DataVersion string `json:"data_version"`
			Description struct {
				DescriptionData []struct {
					Lang  string `json:"lang"`
					Value string `json:"value"`
				} `json:"description_data"`
			} `json:"description"`
			Problemtype struct {
				ProblemtypeData []struct {
					Description []struct {
						Lang  string `json:"lang"`
						Value string `json:"value"`
					} `json:"description"`
				} `json:"problemtype_data"`
			} `json:"problemtype"`
			References struct {
				ReferenceData []struct {
					Name      string        `json:"name"`
					Refsource string        `json:"refsource"`
					Tags      []interface{} `json:"tags"`
					URL       string        `json:"url"`
				} `json:"reference_data"`
			} `json:"references"`
		} `json:"cve"`
		Impact struct {
			BaseMetricV2 struct {
				CvssV2 struct {
					AccessComplexity      string  `json:"accessComplexity"`
					AccessVector          string  `json:"accessVector"`
					Authentication        string  `json:"authentication"`
					AvailabilityImpact    string  `json:"availabilityImpact"`
					BaseScore             float64 `json:"baseScore"`
					ConfidentialityImpact string  `json:"confidentialityImpact"`
					IntegrityImpact       string  `json:"integrityImpact"`
					VectorString          string  `json:"vectorString"`
					Version               string  `json:"version"`
				} `json:"cvssV2"`
				ExploitabilityScore     float64 `json:"exploitabilityScore"`
				ImpactScore             float64 `json:"impactScore"`
				ObtainAllPrivilege      bool    `json:"obtainAllPrivilege"`
				ObtainOtherPrivilege    bool    `json:"obtainOtherPrivilege"`
				ObtainUserPrivilege     bool    `json:"obtainUserPrivilege"`
				Severity                string  `json:"severity"`
				UserInteractionRequired bool    `json:"userInteractionRequired"`
			} `json:"baseMetricV2"`
			BaseMetricV3 struct {
				CvssV3 struct {
					AttackComplexity      string  `json:"attackComplexity"`
					AttackVector          string  `json:"attackVector"`
					AvailabilityImpact    string  `json:"availabilityImpact"`
					BaseScore             float64 `json:"baseScore"`
					BaseSeverity          string  `json:"baseSeverity"`
					ConfidentialityImpact string  `json:"confidentialityImpact"`
					IntegrityImpact       string  `json:"integrityImpact"`
					PrivilegesRequired    string  `json:"privilegesRequired"`
					Scope                 string  `json:"scope"`
					UserInteraction       string  `json:"userInteraction"`
					VectorString          string  `json:"vectorString"`
					Version               string  `json:"version"`
				} `json:"cvssV3"`
				ExploitabilityScore float64 `json:"exploitabilityScore"`
				ImpactScore         float64 `json:"impactScore"`
			} `json:"baseMetricV3"`
		} `json:"impact"`
		LastModifiedDate string `json:"lastModifiedDate"`
		PublishedDate    string `json:"publishedDate"`
	} `json:"CVE_Items"`
	CVEDataFormat       string `json:"CVE_data_format"`
	CVEDataNumberOfCVEs string `json:"CVE_data_numberOfCVEs"`
	CVEDataTimestamp    string `json:"CVE_data_timestamp"`
	CVEDataType         string `json:"CVE_data_type"`
	CVEDataVersion      string `json:"CVE_data_version"`
}

// MyDBdata struct for mongodb data
type MyDBdata struct {
	CVEID               string      `json:"cveid"`
	CWEID               string      `json:"cweid"`
	CPEList             []myCPEitem `json:"cpeitem"`
	Description         []string    `json:"description"`
	References          []string    `json:"references"`
	Score               string      `json:"score"`
	ExploitabilityScore string      `json:"exploitabilityScore"`
}

type myCPEitem struct {
	Cpe22Uri   string `json:"cpe22Uri"`
	Cpe23Uri   string `json:"cpe23Uri"`
	Vulnerable bool   `json:"vulnerable"`
}

type amqpCreds struct {
	Username string
	Password string
	Host     string
	Port     string
}

var amqpcreds amqpCreds

func main() {
	var username = flag.String("u", "", "amqp username")
	var password = flag.String("pwd", "", "amqp password")
	var host = flag.String("h", "", "amqp host")
	var port = flag.String("p", "5672", "amqp port")
	flag.Parse()
	amqpcreds.Host = *host
	amqpcreds.Port = *port
	amqpcreds.Username = *username
	amqpcreds.Password = *password
	log.Print("start")
	DownloadFeeds()
	files, err := ioutil.ReadDir("./feeds")
	if err != nil {
		log.Fatal(err)
	}
	client, ch, queue := rabbitMQInit()
	defer client.Close()
	defer ch.Close()
	var wg sync.WaitGroup
	wg.Add(len(files))
	for _, f := range files {
		go func(f os.FileInfo) {
			defer wg.Done()
			storeData(f.Name(), ch, client, queue)
			err := os.Remove("./feeds/" + f.Name())
			if err != nil {
				log.Fatal(err)
			}
		}(f)
	}
	wg.Wait()
	log.Print("done")
}

// ReadGzFile function for extracting GZ archives
func ReadGzFile(filename string) ([]byte, error) {
	fi, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer fi.Close()
	fz, err := gzip.NewReader(fi)
	if err != nil {
		return nil, err
	}
	defer fz.Close()
	s, err := ioutil.ReadAll(fz)
	if err != nil {
		return nil, err
	}
	return s, nil
}

// DownloadFeeds function for downloading feeds from https://nvd.nist.gov/
func DownloadFeeds() {
	var wg sync.WaitGroup
	urllist := []string{"2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"}
	wg.Add(len(urllist))
	for _, url := range urllist {
		filepath := "./feeds/nvdcve-1.0-" + url + ".json.gz"
		fullurl := "https://nvd.nist.gov/feeds/json/cve/1.0/nvdcve-1.0-" + url + ".json.gz"
		go func(filepath string, fullurl string) {
			defer wg.Done()
			// Create the file
			out, err := os.Create(filepath)
			if err != nil {
				log.Fatal(err)
			}
			defer out.Close()

			// Get the data
			resp, err := http.Get(fullurl)
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()

			// Check server response
			if resp.StatusCode != http.StatusOK {
				log.Fatal(resp.Status)
			}

			// Writer the body to file
			_, err = io.Copy(out, resp.Body)
			if err != nil {
				log.Fatal(err)
			}
		}(filepath, fullurl)
	}
	wg.Wait()

}

func storeData(jsonfile string, ch *amqp.Channel, client *amqp.Connection, queue amqp.Queue) {
	data, err := ReadGzFile("./feeds/" + jsonfile)
	if err != nil {
		log.Fatal(err)
	}
	var c MyJSONName
	err = json.Unmarshal(data, &c)
	if err != nil {
		log.Fatal(err)
	}
	var datalist []MyDBdata
	for _, cveitem := range c.CVEItems {
		mydbdata := MyDBdata{}
		mydbdata.CVEID = cveitem.Cve.CVEDataMeta.ID
		for _, ProblemtypeDataItem := range cveitem.Cve.Problemtype.ProblemtypeData {
			for _, cwe := range ProblemtypeDataItem.Description {
				mydbdata.CWEID = cwe.Value
			}
		}
		mydbdata.Score = strconv.FormatFloat(cveitem.Impact.BaseMetricV2.CvssV2.BaseScore, 'f', 1, 64)
		mydbdata.ExploitabilityScore = strconv.FormatFloat(cveitem.Impact.BaseMetricV3.ExploitabilityScore, 'f', 1, 64)
		for _, description := range cveitem.Cve.Description.DescriptionData {
			mydbdata.Description = append(mydbdata.Description, description.Value)
		}
		for _, reference := range cveitem.Cve.References.ReferenceData {
			mydbdata.References = append(mydbdata.References, reference.URL)
		}
		for _, nodes := range cveitem.Configurations.Nodes {
			for _, cpeitem := range nodes.CpeMatch {
				mydbdata.CPEList = append(mydbdata.CPEList, cpeitem)
			}
		}
		if len(mydbdata.CPEList) > 0 {
			datalist = append(datalist, mydbdata)
		} else if len(cveitem.Configurations.Nodes) == 0 {
			datalist = append(datalist, mydbdata)
		}
	}
	for _, data := range datalist {
		jsonString, err := json.Marshal(data)
		if err != nil {
			log.Fatal("NMAP Struct to Map: ", err)
		}
		rabbitMQConn(jsonString, ch, client, queue)
	}
}

// rabbitMQInit initiates connection to RabbitMQ server
func rabbitMQInit() (*amqp.Connection, *amqp.Channel, amqp.Queue) {
	var rabbiturl string
	rabbiturl = "amqp://" + amqpcreds.Username + ":" + amqpcreds.Password + "@" + amqpcreds.Host + ":" + amqpcreds.Port + "/"
	client, err := amqp.Dial(rabbiturl)
	if err != nil {
		log.Fatal("RABBITMQ Client: ", err)
	}
	ch, err := client.Channel()
	if err != nil {
		log.Fatal("RABBITMQ Channel: ", err)
	}
	queue, err := ch.QueueDeclare(
		"cvedb",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Fatal("RABBITMQ Queue: ", err)
	}
	return client, ch, queue
}

// rabbitMQConn sending data to RabbitMQ server
func rabbitMQConn(data []byte, ch *amqp.Channel, client *amqp.Connection, queue amqp.Queue) {
	body := []byte{}
	body = append(body, data...)
	err := ch.Publish(
		"cvedb",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        data,
		})
	if err != nil {
		log.Fatal("RABBITMQ Connection: ", err)
	}
}

